FROM centbee/android-nodejs

ARG BUILD_DATE
ARG BUILD_VERSION
ARG VCS_REF

LABEL maintainer="lorien@centbee.com" \
	org.label-schema.build-date=$BUILD_DATE \
	org.label-schema.version=$BUILD_VERSION \
	org.label-schema.vcs-ref=$VCS_REF \
	org.label-schema.schema-version="1.0" \
	org.label-schema.name="centbee/cordova" \
	org.label-schema.description="Cordova Image" \
	org.label-schema.vendor="Lorien Gamaroff (Centbee)" \
	org.label-schema.license="MIT" 

ENV CORDOVA_VERSION 9.0.0

WORKDIR "/tmp"

# things

RUN npm i -g --unsafe-perm cordova@${CORDOVA_VERSION}