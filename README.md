# Centbee Cordova image

### Pull newest build from Docker Hub
```
docker pull centbee/cordova:latest
```

### Run image
```
docker run -it centbee/cordova:latest bash
```

### Use as base image
```Dockerfile
FROM centbee/cordova:latest
```
